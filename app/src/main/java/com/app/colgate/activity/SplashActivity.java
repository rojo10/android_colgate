package com.app.colgate.activity;

//import android.arch.persistence.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.colgate.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends AppCompatActivity {


    private long splashDelay = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splasscreen);



        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent a = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(a);
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, splashDelay);



    }

}
