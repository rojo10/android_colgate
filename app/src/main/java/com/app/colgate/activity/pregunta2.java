package com.app.colgate.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.colgate.R;

public class pregunta2 extends AppCompatActivity {
    ImageView img_pre;
    LinearLayout uno,dos;
    Button btnSi,btnNo,btn1,btn2,btn3;
    String preg2;
    int img2,res;
    TextView preg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregunta2);

        Intent i = getIntent();
        img2 = i.getExtras().getInt("img2");
        preg2 = i.getExtras().getString("preg2");
        res = i.getExtras().getInt("res");

        img_pre = (ImageView) findViewById(R.id.img_pre);
        uno = (LinearLayout) findViewById(R.id.uno);
        dos = (LinearLayout) findViewById(R.id.dos);
        btnSi = (Button) findViewById(R.id.btnSi);
        btnNo = (Button) findViewById(R.id.btnNo);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        preg = (TextView) findViewById(R.id.preg);

        if(preg2.equals("¿Cuantas veces al día te cepillas los dientes?")){
            dos.setVisibility(View.VISIBLE);
            uno.setVisibility(View.GONE);
        }else{
            uno.setVisibility(View.VISIBLE);
            dos.setVisibility(View.GONE);
        }
        preg.setText(preg2);

        img_pre.setImageResource(img2);

        btnSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(pregunta2.this , respuesta.class);
                a.putExtra("res",res);
                startActivity(a);
                finish();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(pregunta2.this , respuesta.class);
                a.putExtra("res",res);
                startActivity(a);
                finish();
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(pregunta2.this , respuesta.class);
                a.putExtra("res",res);
                startActivity(a);
                finish();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(pregunta2.this , respuesta.class);
                a.putExtra("res",res);
                startActivity(a);
                finish();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(pregunta2.this , respuesta.class);
                a.putExtra("res",res);
                startActivity(a);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
