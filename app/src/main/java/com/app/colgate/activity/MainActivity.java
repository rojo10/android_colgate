package com.app.colgate.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.app.colgate.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnSi,btnNo;
    private List<Integer> img = new ArrayList<>();
    private List<String> pregun = new ArrayList<>();
    private List<Integer> res = new ArrayList<>();

    List<Integer> num = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num.add(0);
        num.add(1);
        num.add(2);
        num.add(3);
        num.add(4);
        num.add(5);
        num.add(6);
        //img.add(R.mipmap.img1);
        img.add(R.mipmap.img2);
        img.add(R.mipmap.img3);
        img.add(R.mipmap.img4);
        img.add(R.mipmap.img5);
        img.add(R.mipmap.img6);
        img.add(R.mipmap.img7);
        img.add(R.mipmap.img8);

        pregun.add("¿Tomás café?");
        pregun.add("¿Comes dulces?");
        pregun.add("¿Sensibilidad en los dientes?");
        pregun.add("¿Cuantas veces al día te cepillas los dientes?");
        pregun.add("¿Eres fumador?");
        pregun.add("¿Manchas en los dientes?");
        pregun.add("¿Combatir mal aliento?");


        res.add(R.mipmap.res1);
        res.add(R.mipmap.res2);
        res.add(R.mipmap.res3);
        res.add(R.mipmap.res4);
        res.add(R.mipmap.res5);

        btnSi = (Button) findViewById(R.id.btnSi);
        btnNo = (Button) findViewById(R.id.btnNo);

        btnSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.shuffle(num);
                Collections.shuffle(res);
                Log.e("img1",""+img.get(num.get(0)));
                Log.e("img1",""+pregun.get(num.get(0)));
                Log.e("img2",""+img.get(num.get(1)));
                Log.e("img2",""+pregun.get(num.get(1)));
                Log.e("res",""+res.get(0));

                Intent a = new Intent(MainActivity.this , pregunta1.class);
                a.putExtra("img1",img.get(num.get(0)));
                a.putExtra("preg1",""+pregun.get(num.get(0)));
                a.putExtra("img2",img.get(num.get(1)));
                a.putExtra("preg2",""+pregun.get(num.get(1)));
                a.putExtra("res",res.get(0));
                startActivity(a);
                finish();

            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this , GraciasActivity.class);
                startActivity(a);
                finish();
            }
        });
    }
}
