package com.app.colgate.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.app.colgate.R;

public class respuesta extends AppCompatActivity {

    ImageView imgRes;
    int res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respuesta);

        Intent i = getIntent();
        res = i.getExtras().getInt("res");

        imgRes = (ImageView) findViewById(R.id.imgRes);

        imgRes.setImageResource(res);

        imgRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(respuesta.this , GraciasActivity.class);
                startActivity(a);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
